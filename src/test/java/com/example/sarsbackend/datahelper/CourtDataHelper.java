package com.example.sarsbackend.datahelper;

import com.example.sarsbackend.model.Court;
import com.example.sarsbackend.model.SportType;
import com.example.sarsbackend.model.request.CourtRequest;

public class CourtDataHelper {

    public static CourtRequest getCourtRequest() {
        CourtRequest courtRequest = new CourtRequest();
        courtRequest.setName("court name");
        courtRequest.setSportType(SportType.VOLLEYBALL.toString());
        courtRequest.setBuilding(":)");
        return courtRequest;
    }

    public static Court getCourt() {
        Court court = new Court();
        court.setId(1L);
        court.setName("First baskteball court");
        court.setSportType(SportType.FOOTBALL);
        return court;
    }
}
