package com.example.sarsbackend.datahelper;

import com.example.sarsbackend.model.Service;
import com.example.sarsbackend.model.request.ServiceRequest;

import static com.example.sarsbackend.datahelper.CourtDataHelper.getCourt;
import static com.example.sarsbackend.datahelper.EquipmentDataHelper.getEquipmentSet;

public class ServiceDataHelper {

    public static ServiceRequest getServiceRequest() {
        ServiceRequest serviceRequest = new ServiceRequest();

        serviceRequest.setName("volleyball pack");
        serviceRequest.setCourtId(4L);

        return serviceRequest;
    }

    public static Service getService() {
        Service service = new Service();

        service.setId(5L);
        service.setName("the service!");
        service.setEquipmentSet(getEquipmentSet());
        service.setCourt(getCourt());

        return service;
    }
}
