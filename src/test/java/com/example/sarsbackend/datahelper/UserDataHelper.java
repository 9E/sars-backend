package com.example.sarsbackend.datahelper;

import com.example.sarsbackend.model.Role;
import com.example.sarsbackend.model.User;

public class UserDataHelper {

    public static User getUser() {
        User user = new User();
        user.setId(1L);
        user.setUsername("username");
        user.setPassword("password");
        user.setRole(Role.USER);
        return user;
    }

    public static User getAnotherUser() {
        User user = new User();
        user.setUsername("another_username");
        user.setPassword("another_password");
        user.setRole(Role.USER);
        return user;
    }
}
