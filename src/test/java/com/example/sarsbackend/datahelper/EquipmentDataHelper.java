package com.example.sarsbackend.datahelper;

import com.example.sarsbackend.model.Equipment;

import java.util.Set;

public class EquipmentDataHelper {

    public static Equipment getEquipment1() {
        Equipment equipment = new Equipment();

        equipment.setId(6L);
        equipment.setName("tennis racket");
        equipment.setPricePerHour(20.00);

        return equipment;
    }

    public static Equipment getEquipment2() {
        Equipment equipment = new Equipment();

        equipment.setId(7L);
        equipment.setName("Golf club");

        return equipment;
    }

    public static Set<Equipment> getEquipmentSet() {
        return Set.of(
                getEquipment1(),
                getEquipment2()
        );
    }
}
