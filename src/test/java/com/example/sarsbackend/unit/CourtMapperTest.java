package com.example.sarsbackend.unit;

import com.example.sarsbackend.mapper.CourtMapper;
import com.example.sarsbackend.model.Court;
import com.example.sarsbackend.model.request.CourtRequest;
import com.example.sarsbackend.model.response.CourtResponse;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static com.example.sarsbackend.datahelper.CourtDataHelper.getCourt;
import static com.example.sarsbackend.datahelper.CourtDataHelper.getCourtRequest;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CourtMapperTest {
    private final CourtMapper courtMapper = Mappers.getMapper(CourtMapper.class);

    @Test
    void shouldMapCourtRequestToCourt() {
        // given
        CourtRequest courtRequest = getCourtRequest();

        // when
        Court actual = courtMapper.courtRequestToCourt(courtRequest);

        // then
        assertEquals(courtRequest.getName(), actual.getName());
        assertEquals(courtRequest.getBuilding(), actual.getBuilding());
        assertEquals(courtRequest.getSportType(), actual.getSportType().toString());
    }

    @Test
    void shouldMapCourtToCourtResponse() {
        // given
        Court court = getCourt();

        // when
        CourtResponse actual = courtMapper.courtToCourtResponse(court);

        // then
        assertEquals(court.getId(), court.getId());
        assertEquals(court.getSportType(), court.getSportType());
        assertEquals(court.getName(), actual.getName());
    }
}