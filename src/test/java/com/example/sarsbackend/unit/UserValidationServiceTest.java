package com.example.sarsbackend.unit;

import com.example.sarsbackend.exception.AccessForbiddenException;
import com.example.sarsbackend.model.User;
import com.example.sarsbackend.service.UserService;
import com.example.sarsbackend.service.UserValidationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;

import static com.example.sarsbackend.datahelper.UserDataHelper.getUser;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

public class UserValidationServiceTest {

    private UserValidationService userValidationService;
    @Mock
    private Authentication authentication;
    @Mock
    private UserService userService;

    @BeforeEach
    void init() {
        MockitoAnnotations.openMocks(this);
        userValidationService = new UserValidationService(userService);
    }

    @Test
    void shouldThrowAccessForbiddenExceptionIfUser() {
        // given
        User user = getUser();
        List authorities = List.of(new SimpleGrantedAuthority("USER"));

        given(authentication.getAuthorities()).willReturn(authorities);
        given(userService.findByUsername(any())).willReturn(user);

        // then
        assertThrows(AccessForbiddenException.class,
                () -> userValidationService.validateAccessToUsersResource(authentication, 2L));
    }

    @Test
    void shouldNotThrowAccessForbiddenExceptionIfUser() {
        // given
        User user = getUser();
        List authorities = List.of(new SimpleGrantedAuthority("USER"));

        given(authentication.getAuthorities()).willReturn(authorities);
        given(userService.findByUsername(any())).willReturn(user);

        // then
        assertDoesNotThrow(() -> userValidationService.validateAccessToUsersResource(authentication, 1L));
    }

    @Test
    void shouldNotThrowAccessForbiddenExceptionIfAdmin() {
        // given
        List authorities = List.of(new SimpleGrantedAuthority("ADMIN"));
        given(authentication.getAuthorities()).willReturn(authorities);

        // then
        assertDoesNotThrow(() -> userValidationService.validateAccessToUsersResource(authentication, 1L));
    }

    @Test
    void shouldReturnIsAdmin() {
        // given
        List authorities = List.of(new SimpleGrantedAuthority("ADMIN"));
        given(authentication.getAuthorities()).willReturn(authorities);

        // then
        assertTrue(userValidationService.isAdmin(authentication));
    }

    @Test
    void shouldReturnIsNotAdmin() {
        // given
        List authorities = List.of(new SimpleGrantedAuthority("USER"));
        given(authentication.getAuthorities()).willReturn(authorities);

        // then
        assertFalse(userValidationService.isAdmin(authentication));
    }
}
