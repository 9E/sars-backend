package com.example.sarsbackend.unit;

import com.example.sarsbackend.mapper.ServiceMapper;
import com.example.sarsbackend.model.Service;
import com.example.sarsbackend.model.request.ServiceRequest;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static com.example.sarsbackend.datahelper.ServiceDataHelper.getServiceRequest;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ServiceMapperTest {
    private final ServiceMapper serviceMapper = Mappers.getMapper(ServiceMapper.class);

    @Test
    void shouldMapServiceRequestToServiceWithCourtIdAndEquipmentIds() {
        // given
        ServiceRequest serviceRequest = getServiceRequest();

        // when
        Service service = serviceMapper.serviceRequestToService(serviceRequest);

        // then
        assertEquals(serviceRequest.getName(), service.getName());
        assertEquals(serviceRequest.getCourtId(), service.getCourt().getId());
    }

    @Test
    void shouldMapServiceRequestToServiceWithNullCourtIdAndNullEquipmentIds() {
        // given
        ServiceRequest serviceRequest = new ServiceRequest();

        // when
        Service service = serviceMapper.serviceRequestToService(serviceRequest);

        // then
        assertEquals(serviceRequest.getName(), service.getName());
        assertEquals(serviceRequest.getCourtId(), service.getCourt().getId());
    }
}