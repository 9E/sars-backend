package com.example.sarsbackend.unit;

import com.example.sarsbackend.mapper.UserMapper;
import com.example.sarsbackend.model.User;
import com.example.sarsbackend.model.response.UserResponse;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static com.example.sarsbackend.datahelper.UserDataHelper.getUser;
import static org.junit.jupiter.api.Assertions.assertEquals;

class UserMapperTest {
    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    @Test
    void shouldMapUserToUserResponse() {
        // given
        User user = getUser();

        // when
        UserResponse actual = userMapper.userToUserResponse(user);

        // then
        assertEquals(user.getUsername(), actual.getUsername());
        assertEquals(user.getRole(), actual.getRole());
    }
}