package com.example.sarsbackend.integration;

import com.example.sarsbackend.model.Role;
import com.example.sarsbackend.model.User;
import com.example.sarsbackend.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

import static com.example.sarsbackend.datahelper.UserDataHelper.getAnotherUser;
import static com.example.sarsbackend.datahelper.UserDataHelper.getUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
public class UserServiceTest {

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserService userService;

    @Test
    void shouldCreateUser() {
        // given
        User expectedUser = getUser();
        String unencryptedPassword = expectedUser.getPassword();

        // when
        User actualUser = userService.save(expectedUser);

        // then
        assertEquals(expectedUser.getUsername(), actualUser.getUsername());
        assertTrue(passwordEncoder.matches(unencryptedPassword, actualUser.getPassword()));
        assertEquals(Role.USER, actualUser.getRole());
    }

    @Test
    void shouldUpdateUser() {
        // given
        User initialUser = getUser();
        User anotherUser = getAnotherUser();
        String unencryptedPassword = anotherUser.getPassword();

        // when
        User savedInitialUser = userService.save(initialUser);
        savedInitialUser.setPassword(unencryptedPassword);
        User updatedUser = userService.update(savedInitialUser.getId(), anotherUser);

        // then
        assertEquals(savedInitialUser.getId(), updatedUser.getId());
        assertEquals(anotherUser.getUsername(), updatedUser.getUsername());
        assertTrue(passwordEncoder.matches(unencryptedPassword, updatedUser.getPassword()));
        assertEquals(Role.USER, updatedUser.getRole());
    }
}
