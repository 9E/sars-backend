package com.example.sarsbackend.util;

public class Constants {

    public static final int MIN_BASKETBALL_TEAM_SIZE = 12;
    public static final int MAX_BASKETBALL_TEAM_SIZE = 15;
    public static final int MIN_FOOTBALL_TEAM_SIZE = 11;
    public static final int MAX_FOOTBALL_TEAM_SIZE = 18;
    public static final int MIN_VOLLEYBALL_TEAM_SIZE = 6;
    public static final int MAX_VOLLEYBALL_TEAM_SIZE = 12;
}
