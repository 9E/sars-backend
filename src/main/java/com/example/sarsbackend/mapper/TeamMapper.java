package com.example.sarsbackend.mapper;

import com.example.sarsbackend.model.Team;
import com.example.sarsbackend.model.request.TeamRequest;
import com.example.sarsbackend.model.response.TeamResponse;
import com.example.sarsbackend.model.response.TeamSlimDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TeamMapper {

    Team teamRequestToTeam(TeamRequest teamRequest);

    List<TeamResponse> teamsToTeamResponses(List<Team> teams);

    TeamResponse teamToTeamResponse(Team team);
}
