package com.example.sarsbackend.mapper;

import com.example.sarsbackend.model.User;
import com.example.sarsbackend.model.request.UserRequest;
import com.example.sarsbackend.model.response.UserResponse;
import com.example.sarsbackend.model.response.UserSlimDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User userRequestToUser(UserRequest userRequest);

    List<UserResponse> usersToUserResponses(List<User> users);

    UserResponse userToUserResponse(User user);

    UserSlimDto userToUserSlimDto(User user);
}
