package com.example.sarsbackend.mapper;

import com.example.sarsbackend.model.Reservation;
import com.example.sarsbackend.model.request.ReservationRequest;
import com.example.sarsbackend.model.response.ReservationResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Mapper(componentModel = "spring")
public interface ReservationMapper {

    @Mapping(source = "userId", target = "user.id")
    @Mapping(source = "serviceId", target = "service.id")
    Reservation reservationRequestToReservation(ReservationRequest reservationRequest);

    List<ReservationResponse> reservationsToReservationResponses(List<Reservation> reservations);

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "service.id", target = "serviceId")
    @Mapping(source = "totalPrice", target = "totalPrice", qualifiedByName = "totalPriceToPrecision2")
    ReservationResponse reservationToReservationResponse(Reservation reservation);

    @Named("totalPriceToPrecision2")
    default double totalPriceToPrecision2(Double aDouble) {
        BigDecimal bigDecimal = new BigDecimal(aDouble);
        BigDecimal scaledBigDecimal = bigDecimal.setScale(2, RoundingMode.HALF_UP);
        return scaledBigDecimal.doubleValue();
    }
}
