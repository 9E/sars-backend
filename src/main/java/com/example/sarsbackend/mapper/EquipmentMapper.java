package com.example.sarsbackend.mapper;

import com.example.sarsbackend.model.Equipment;
import com.example.sarsbackend.model.request.EquipmentRequest;
import com.example.sarsbackend.model.response.EquipmentResponse;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EquipmentMapper {

    Equipment equipmentRequestToEquipment(EquipmentRequest equipmentRequest);

    List<EquipmentResponse> equipmentListToEquipmentResponses(List<Equipment> equipmentList);

    EquipmentResponse equipmentToEquipmentResponse(Equipment equipment);
}
