package com.example.sarsbackend.mapper;

import com.example.sarsbackend.model.Service;
import com.example.sarsbackend.model.request.ServiceRequest;
import com.example.sarsbackend.model.response.ServiceResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ServiceMapper {

    @Mapping(source = "courtId", target = "court.id")
    Service serviceRequestToService(ServiceRequest serviceRequest);

    List<ServiceResponse> servicesToServiceResponses(List<Service> services);

    ServiceResponse serviceToServiceResponse(Service service);
}
