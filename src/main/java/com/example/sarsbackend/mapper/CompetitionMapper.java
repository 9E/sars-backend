package com.example.sarsbackend.mapper;

import com.example.sarsbackend.model.Competition;
import com.example.sarsbackend.model.request.CompetitionRequest;
import com.example.sarsbackend.model.response.CompetitionResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CompetitionMapper {

    @Mapping(source = "team1Id", target = "team1.id")
    @Mapping(source = "team2Id", target = "team2.id")
    @Mapping(source = "courtId", target = "court.id")
    Competition competitionRequestToCompetition(CompetitionRequest competitionRequest);

    List<CompetitionResponse> competitionsToCompetitionResponses(List<Competition> competitions);

    CompetitionResponse competitionToCompetitionResponse(Competition competition);
}
