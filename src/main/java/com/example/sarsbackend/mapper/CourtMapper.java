package com.example.sarsbackend.mapper;

import com.example.sarsbackend.model.Court;
import com.example.sarsbackend.model.request.CourtRequest;
import com.example.sarsbackend.model.response.CourtResponse;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CourtMapper {

    Court courtRequestToCourt(CourtRequest courtRequest);

    List<CourtResponse> courtsToCourtResponses(List<Court> courts);

    CourtResponse courtToCourtResponse(Court court);
}
