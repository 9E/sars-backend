package com.example.sarsbackend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "teams")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String name;
    private SportType sportType;
    @ManyToMany
    @JoinTable(name = "team_members",
            joinColumns = {@JoinColumn(name = "team_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> members;
    @ManyToOne
    private User createdBy;
    @OneToMany(mappedBy = "team1")
    private Set<Competition> competitionsAsTeam1;
    @OneToMany(mappedBy = "team2")
    private Set<Competition> competitionsAsTeam2;
}
