package com.example.sarsbackend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String username;
    private String password;
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;
    private String firstName;
    private String lastName;
    private String email;
    private Timestamp createdAt;
    @OneToMany(mappedBy = "user")
    private Set<Reservation> reservations;
    @ManyToMany(mappedBy = "members")
    private Set<Team> teams;
    @OneToMany(mappedBy = "createdBy")
    private Set<Team> createdTeams;

    public User(String username, String password, Role role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }
}
