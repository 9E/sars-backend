package com.example.sarsbackend.model;

public enum SportType {
    VOLLEYBALL,
    FOOTBALL,
    BASKETBALL
}
