package com.example.sarsbackend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "courts")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Court {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Enumerated(value = EnumType.STRING)
    private SportType sportType;
    private String building;
    @OneToMany(mappedBy = "court")
    private Set<Service> services;
    private Double pricePerHour;
    @OneToMany(mappedBy = "court")
    private Set<Competition> competitions;
}
