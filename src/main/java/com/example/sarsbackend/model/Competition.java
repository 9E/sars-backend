package com.example.sarsbackend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "competitions")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Competition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private SportType sportType;
    @ManyToOne
    private Team team1;
    @ManyToOne
    private Team team2;
    @ManyToOne
    private Court court;
    private Timestamp dateFrom;
    private Timestamp dateTo;
}
