package com.example.sarsbackend.model;

public enum Role {
    USER,
    ADMIN
}
