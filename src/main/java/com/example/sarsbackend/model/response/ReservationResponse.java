package com.example.sarsbackend.model.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;
import java.util.Set;

@Getter
@Setter
public class ReservationResponse {

    private Long id;
    private Double totalPrice;
    private Long userId;
    private Long serviceId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Timestamp reservedFrom;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Timestamp reservedTo;
}
