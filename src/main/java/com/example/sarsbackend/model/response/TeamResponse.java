package com.example.sarsbackend.model.response;

import com.example.sarsbackend.model.SportType;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class TeamResponse {

    private Long id;
    private String name;
    private SportType sportType;
    private Set<UserSlimDto> members;
    private UserSlimDto createdBy;
}
