package com.example.sarsbackend.model.response;

import com.example.sarsbackend.model.SportType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class CompetitionResponse {

    private Long id;
    private String name;
    private SportType sportType;
    private TeamSlimDto team1;
    private TeamSlimDto team2;
    private CourtSlimDto court;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Timestamp dateFrom;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Timestamp dateTo;
}
