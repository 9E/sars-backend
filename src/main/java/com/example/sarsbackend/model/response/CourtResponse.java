package com.example.sarsbackend.model.response;

import com.example.sarsbackend.model.SportType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourtResponse {

    private Long id;
    private String name;
    private SportType sportType;
    private String building;
}
