package com.example.sarsbackend.model.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class ServiceResponse {

    private Long id;
    private String name;
    private Set<EquipmentResponse> equipmentSet;
    private CourtResponse court;
}
