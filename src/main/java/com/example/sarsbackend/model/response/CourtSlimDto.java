package com.example.sarsbackend.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourtSlimDto {

    private Long id;
    private String name;
}
