package com.example.sarsbackend.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EquipmentResponse {

    private Long id;
    private String name;
    private Double pricePerHour;
}
