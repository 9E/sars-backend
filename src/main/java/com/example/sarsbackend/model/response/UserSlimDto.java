package com.example.sarsbackend.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserSlimDto {

    private Long id;
    private String username;
}
