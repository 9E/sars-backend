package com.example.sarsbackend.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamSlimDto {
    private Long id;
    private String name;
}
