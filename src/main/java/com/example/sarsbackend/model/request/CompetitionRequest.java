package com.example.sarsbackend.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;

@Getter
@Setter
public class CompetitionRequest {

    @NotEmpty(message = "Name can't be empty")
    private String name;
    @NotEmpty(message = "Sport type can't be empty")
    @Pattern(regexp = "^(VOLLEYBALL)|(FOOTBALL)|(BASKETBALL)$",
            message = "Sport type must be VOLLEYBALL, FOOTBALL or BASKETBALL")
    private String sportType;
    @NotNull(message = "Team1 id can't be empty")
    private Long team1Id;
    @NotNull(message = "Team2 id can't be empty")
    private Long team2Id;
    @NotNull(message = "Court id can't be empty")
    private Long courtId;
    @NotNull(message = "Date from can't be empty")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Timestamp dateFrom;
    @NotNull(message = "Date to can't be empty")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Timestamp dateTo;
}
