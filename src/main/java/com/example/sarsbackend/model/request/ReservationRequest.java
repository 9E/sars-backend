package com.example.sarsbackend.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Getter
@Setter
public class ReservationRequest {

    @NotNull(message = "User id can't be empty")
    private Long userId;
    @NotNull(message = "Service id can't be empty")
    private Long serviceId;
    @NotNull(message = "Reserved from date can't be empty")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Timestamp reservedFrom;
    @NotNull(message = "Reserved to date can't be empty")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Timestamp reservedTo;
}
