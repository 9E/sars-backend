package com.example.sarsbackend.model.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class EquipmentRequest {

    @NotEmpty(message = "Equipment name can't be empty")
    private String name;
    private Double pricePerHour;
}
