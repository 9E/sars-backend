package com.example.sarsbackend.model.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ServiceRequest {

    @NotEmpty(message = "Service name can't be empty")
    private String name;
    private Long courtId;
}
