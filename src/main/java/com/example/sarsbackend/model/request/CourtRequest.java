package com.example.sarsbackend.model.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Getter
@Setter
public class CourtRequest {

    @NotEmpty(message = "Court name can't be empty")
    private String name;
    @NotEmpty(message = "Sport type can't be empty")
    @Pattern(regexp = "^(VOLLEYBALL)|(FOOTBALL)|(BASKETBALL)$",
            message = "Sport type must be VOLLEYBALL, FOOTBALL or BASKETBALL")
    private String sportType;
    private String building;
}
