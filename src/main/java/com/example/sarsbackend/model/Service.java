package com.example.sarsbackend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "services")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToMany(mappedBy = "service")
    private Set<Equipment> equipmentSet = new HashSet<>();
    @ManyToOne
    private Court court;
    @OneToMany(mappedBy = "service")
    private Set<Reservation> reservations = new HashSet<>();
}
