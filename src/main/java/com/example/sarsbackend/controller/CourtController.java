package com.example.sarsbackend.controller;

import com.example.sarsbackend.mapper.CourtMapper;
import com.example.sarsbackend.model.Court;
import com.example.sarsbackend.model.request.CourtRequest;
import com.example.sarsbackend.model.response.CourtResponse;
import com.example.sarsbackend.service.CourtService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/courts")
@RequiredArgsConstructor
@Validated
public class CourtController {
    private final CourtService courtService;
    private final CourtMapper courtMapper;

    @GetMapping
    public ResponseEntity<List<CourtResponse>> getCourts() {
        List<Court> courts = courtService.findAll();
        return ResponseEntity.ok(courtMapper.courtsToCourtResponses(courts));
    }

    @GetMapping("/{id}")
    public ResponseEntity<CourtResponse> getCourt(@PathVariable Long id) {
        Court court = courtService.findById(id);
        return ResponseEntity.ok(courtMapper.courtToCourtResponse(court));
    }

    @PostMapping
    public ResponseEntity<CourtResponse> createCourt(@Valid @RequestBody CourtRequest courtRequest) {
        Court courtToAdd = courtMapper.courtRequestToCourt(courtRequest);
        Court createdCourt = courtService.save(courtToAdd);
        CourtResponse courtResponse = courtMapper.courtToCourtResponse(createdCourt);
        return ResponseEntity.status(HttpStatus.CREATED).body(courtResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CourtResponse> updateCourt(@PathVariable Long id,
                                                     @Valid @RequestBody CourtRequest courtRequest) {
        Court court = courtMapper.courtRequestToCourt(courtRequest);
        Court updatedCourt = courtService.update(id, court);
        return ResponseEntity.ok(courtMapper.courtToCourtResponse(updatedCourt));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<CourtResponse> deleteCourt(@PathVariable Long id) {
        courtService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
