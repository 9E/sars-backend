package com.example.sarsbackend.controller;

import com.example.sarsbackend.mapper.UserMapper;
import com.example.sarsbackend.model.User;
import com.example.sarsbackend.model.request.UserRequest;
import com.example.sarsbackend.model.response.UserResponse;
import com.example.sarsbackend.service.UserService;
import com.example.sarsbackend.service.UserValidationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
@Validated
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final UserValidationService userValidationService;

    @GetMapping
    public ResponseEntity<List<UserResponse>> getUsers() {
        List<User> users = userService.findAll();
        List<UserResponse> userResponses = userMapper.usersToUserResponses(users);
        return ResponseEntity.ok(userResponses);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> getUser(Authentication authentication, @PathVariable Long id) {
        userValidationService.validateAccessToUsersResource(authentication, id);
        User user = userService.findById(id);
        UserResponse userResponse = userMapper.userToUserResponse(user);
        return ResponseEntity.ok(userResponse);
    }

    @GetMapping("/by-username/{username}")
    public ResponseEntity<UserResponse> getUserByUsername(@PathVariable String username) {
        User user = userService.findByUsername(username);
        UserResponse userResponse = userMapper.userToUserResponse(user);
        return ResponseEntity.ok(userResponse);
    }

    @PostMapping
    public ResponseEntity<UserResponse> postUser(@RequestBody UserRequest userRequest) {
        User userToAdd = userMapper.userRequestToUser(userRequest);
        User createdUser = userService.save(userToAdd);
        UserResponse userResponse = userMapper.userToUserResponse(createdUser);
        return ResponseEntity.status(HttpStatus.CREATED).body(userResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserResponse> updateUser(Authentication authentication,
                                                   @PathVariable Long id, @RequestBody UserRequest userRequest) {
        userValidationService.validateAccessToUsersResource(authentication, id);
        User user = userMapper.userRequestToUser(userRequest);
        User updatedUser = userService.update(id, user);
        return ResponseEntity.ok(userMapper.userToUserResponse(updatedUser));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteUser(Authentication authentication, @PathVariable Long id) {
        userValidationService.validateAccessToUsersResource(authentication, id);
        userService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("{id}/role/{roleName}")
    public ResponseEntity<UserResponse> updateUserRole(@PathVariable Long id,
                                                       @PathVariable
                                                       @Pattern(regexp = "^(USER)|(ADMIN)$",
                                                               message = "Role must be USER or ADMIN")
                                                               String roleName) {
        User user = userService.updateRole(id, roleName);
        return ResponseEntity.ok(userMapper.userToUserResponse(user));
    }
}
