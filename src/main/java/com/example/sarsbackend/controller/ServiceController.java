package com.example.sarsbackend.controller;

import com.example.sarsbackend.mapper.ServiceMapper;
import com.example.sarsbackend.model.Service;
import com.example.sarsbackend.model.request.ServiceRequest;
import com.example.sarsbackend.model.response.ServiceResponse;
import com.example.sarsbackend.service.ServiceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/services")
@RequiredArgsConstructor
public class ServiceController {
    private final ServiceService serviceService;
    private final ServiceMapper serviceMapper;

    @GetMapping
    public ResponseEntity<List<ServiceResponse>> getServices() {
        List<Service> services = serviceService.findAll();
        return ResponseEntity.ok(serviceMapper.servicesToServiceResponses(services));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ServiceResponse> getService(@PathVariable Long id) {
        Service service = serviceService.findById(id);
        return ResponseEntity.ok(serviceMapper.serviceToServiceResponse(service));
    }

    @PostMapping
    public ResponseEntity<ServiceResponse> createService(@Valid @RequestBody ServiceRequest serviceRequest) {
        Service serviceToAdd = serviceMapper.serviceRequestToService(serviceRequest);
        Service createdService = serviceService.save(serviceToAdd);
        ServiceResponse serviceResponse = serviceMapper.serviceToServiceResponse(createdService);
        return ResponseEntity.status(HttpStatus.CREATED).body(serviceResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ServiceResponse> updateService(@PathVariable Long id,
                                                     @Valid @RequestBody ServiceRequest serviceRequest) {
        Service service = serviceMapper.serviceRequestToService(serviceRequest);
        Service updatedService = serviceService.update(id, service);
        return ResponseEntity.ok(serviceMapper.serviceToServiceResponse(updatedService));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ServiceResponse> deleteService(@PathVariable Long id) {
        serviceService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("{serviceId}/equipment/{equipmentId}")
    public ResponseEntity<ServiceResponse> updateServiceEquipment(@PathVariable Long serviceId,
                                                                  @PathVariable Long equipmentId) {
        Service service = serviceService.updateEquipment(serviceId, equipmentId);
        return ResponseEntity.ok(serviceMapper.serviceToServiceResponse(service));
    }

    @DeleteMapping("{serviceId}/equipment/{equipmentId}")
    public ResponseEntity<ServiceResponse> deleteServiceEquipment(@PathVariable Long serviceId,
                                                                  @PathVariable Long equipmentId) {
        Service service = serviceService.deleteEquipment(serviceId, equipmentId);
        return ResponseEntity.ok(serviceMapper.serviceToServiceResponse(service));
    }
}
