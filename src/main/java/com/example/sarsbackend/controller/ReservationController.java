package com.example.sarsbackend.controller;

import com.example.sarsbackend.mapper.ReservationMapper;
import com.example.sarsbackend.model.Reservation;
import com.example.sarsbackend.model.request.ReservationRequest;
import com.example.sarsbackend.model.response.ReservationResponse;
import com.example.sarsbackend.service.ReservationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/reservations")
@RequiredArgsConstructor
public class ReservationController {
    private final ReservationService reservationService;
    private final ReservationMapper reservationMapper;

    @GetMapping
    public ResponseEntity<List<ReservationResponse>> getReservations() {
        List<Reservation> reservations = reservationService.findAll();
        return ResponseEntity.ok(reservationMapper.reservationsToReservationResponses(reservations));
    }

    @PostMapping
    public ResponseEntity<ReservationResponse> reserveService(@Valid @RequestBody ReservationRequest reservationRequest) {
        Reservation reservation = reservationMapper.reservationRequestToReservation(reservationRequest);
        Reservation createdReservation = reservationService.createReservation(reservation);
        ReservationResponse reservationResponse = reservationMapper
                .reservationToReservationResponse(createdReservation);
        return ResponseEntity.status(HttpStatus.CREATED).body(reservationResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ReservationResponse> deleteReservation(@PathVariable Long id) {
        reservationService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
