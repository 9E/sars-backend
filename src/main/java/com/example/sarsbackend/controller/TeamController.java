package com.example.sarsbackend.controller;

import com.example.sarsbackend.mapper.TeamMapper;
import com.example.sarsbackend.model.Team;
import com.example.sarsbackend.model.User;
import com.example.sarsbackend.model.request.TeamRequest;
import com.example.sarsbackend.model.response.TeamResponse;
import com.example.sarsbackend.service.TeamService;
import com.example.sarsbackend.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/teams")
@RequiredArgsConstructor
@Validated
public class TeamController {
    private final TeamService teamService;
    private final TeamMapper teamMapper;
    private final UserService userService;

    @GetMapping
    public ResponseEntity<List<TeamResponse>> getTeams() {
        List<Team> teams = teamService.findAll();
        return ResponseEntity.ok(teamMapper.teamsToTeamResponses(teams));
    }

    @GetMapping("/{id}")
    public ResponseEntity<TeamResponse> getTeam(@PathVariable Long id) {
        Team team = teamService.findById(id);
        return ResponseEntity.ok(teamMapper.teamToTeamResponse(team));
    }

    @PostMapping
    public ResponseEntity<TeamResponse> createTeam(@Valid @RequestBody TeamRequest teamRequest,
                                                   Authentication authentication) {
        Team teamToAdd = teamMapper.teamRequestToTeam(teamRequest);
        User authenticatedUser = userService.findByUsername(authentication.getName());
        Team createdTeam = teamService.save(teamToAdd, authenticatedUser);
        TeamResponse teamResponse = teamMapper.teamToTeamResponse(createdTeam);
        return ResponseEntity.status(HttpStatus.CREATED).body(teamResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TeamResponse> updateTeam(@PathVariable Long id,
                                                     @Valid @RequestBody TeamRequest teamRequest) {
        Team team = teamMapper.teamRequestToTeam(teamRequest);
        Team updatedTeam = teamService.update(id, team);
        return ResponseEntity.ok(teamMapper.teamToTeamResponse(updatedTeam));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<TeamResponse> deleteTeam(@PathVariable Long id) {
        teamService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
