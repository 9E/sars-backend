package com.example.sarsbackend.controller;

import com.example.sarsbackend.mapper.UserMapper;
import com.example.sarsbackend.model.User;
import com.example.sarsbackend.model.response.UserResponse;
import com.example.sarsbackend.service.TeamMembersService;
import com.example.sarsbackend.service.TeamService;
import com.example.sarsbackend.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Validated
public class TeamMembersController {
    private final UserService userService;
    private final TeamService teamService;
    private final TeamMembersService teamMembersService;
    private final UserMapper userMapper;

    @PutMapping("/user/{userId}/teams")
    public ResponseEntity<UserResponse> getUserBelongingToTeams(@PathVariable Long userId) {
        User user = userService.findById(userId);
        return ResponseEntity.ok(userMapper.userToUserResponse(user));
    }

    @PutMapping("/user/{userId}/teams/{teamId}")
    public ResponseEntity<UserResponse> addTeamToUser(@PathVariable Long userId, @PathVariable Long teamId) {
        User user = teamMembersService.addTeamToUser(userId, teamId);
        return ResponseEntity.ok(userMapper.userToUserResponse(user));
    }

    @DeleteMapping("/user/{userId}/teams/{teamId}")
    public ResponseEntity<Object> removeTeamFromUser(@PathVariable Long userId, @PathVariable Long teamId) {
        User user = teamMembersService.removeTeamFromUser(userId, teamId);
        return ResponseEntity.ok(null);
    }
}
