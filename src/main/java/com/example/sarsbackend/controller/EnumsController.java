package com.example.sarsbackend.controller;

import com.example.sarsbackend.model.SportType;
import com.example.sarsbackend.model.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class EnumsController {

    @GetMapping("/roles")
    public ResponseEntity<List<Role>> getRoles() {
        return ResponseEntity.ok(List.of(Role.values()));
    }

    @GetMapping( "/sport-types")
    public ResponseEntity<List<SportType>> getSportTypes() {
        return ResponseEntity.ok(List.of(SportType.values()));
    }
}
