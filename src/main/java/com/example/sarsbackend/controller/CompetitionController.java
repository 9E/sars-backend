package com.example.sarsbackend.controller;

import com.example.sarsbackend.mapper.CompetitionMapper;
import com.example.sarsbackend.model.Competition;
import com.example.sarsbackend.model.request.CompetitionRequest;
import com.example.sarsbackend.model.response.CompetitionResponse;
import com.example.sarsbackend.service.CompetitionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/competitions")
@RequiredArgsConstructor
@Validated
public class CompetitionController {
    private final CompetitionService competitionService;
    private final CompetitionMapper competitionMapper;

    @GetMapping
    public ResponseEntity<List<CompetitionResponse>> getCompetitions() {
        List<Competition> competitions = competitionService.findAll();
        return ResponseEntity.ok(competitionMapper.competitionsToCompetitionResponses(competitions));
    }

    @GetMapping("/{id}")
    public ResponseEntity<CompetitionResponse> getCompetition(@PathVariable Long id) {
        Competition competition = competitionService.findById(id);
        return ResponseEntity.ok(competitionMapper.competitionToCompetitionResponse(competition));
    }

    @PostMapping
    public ResponseEntity<CompetitionResponse> createCompetition(
            @Valid @RequestBody CompetitionRequest competitionRequest) {
        Competition competitionToAdd = competitionMapper.competitionRequestToCompetition(competitionRequest);
        Competition createdCompetition = competitionService.save(competitionToAdd);
        CompetitionResponse competitionResponse = competitionMapper.competitionToCompetitionResponse(createdCompetition);
        return ResponseEntity.status(HttpStatus.CREATED).body(competitionResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CompetitionResponse> updateCompetition(
            @PathVariable Long id, @Valid @RequestBody CompetitionRequest competitionRequest) {
        Competition competition = competitionMapper.competitionRequestToCompetition(competitionRequest);
        Competition updatedCompetition = competitionService.update(id, competition);
        return ResponseEntity.ok(competitionMapper.competitionToCompetitionResponse(updatedCompetition));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<CompetitionResponse> deleteCompetition(@PathVariable Long id) {
        competitionService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
