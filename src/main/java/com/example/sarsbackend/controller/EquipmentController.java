package com.example.sarsbackend.controller;

import com.example.sarsbackend.mapper.EquipmentMapper;
import com.example.sarsbackend.model.Equipment;
import com.example.sarsbackend.model.request.EquipmentRequest;
import com.example.sarsbackend.model.response.EquipmentResponse;
import com.example.sarsbackend.service.EquipmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/equipment")
@RequiredArgsConstructor
public class EquipmentController {
    private final EquipmentService equipmentService;
    private final EquipmentMapper equipmentMapper;

    @GetMapping
    public ResponseEntity<List<EquipmentResponse>> getEquipments() {
        List<Equipment> equipments = equipmentService.findAll();
        return ResponseEntity.ok(equipmentMapper.equipmentListToEquipmentResponses(equipments));
    }

    @GetMapping("/{id}")
    public ResponseEntity<EquipmentResponse> getEquipment(@PathVariable Long id) {
        Equipment equipment = equipmentService.findById(id);
        return ResponseEntity.ok(equipmentMapper.equipmentToEquipmentResponse(equipment));
    }

    @PostMapping
    public ResponseEntity<EquipmentResponse> createEquipment(@Valid @RequestBody EquipmentRequest equipmentRequest) {
        Equipment equipmentToAdd = equipmentMapper.equipmentRequestToEquipment(equipmentRequest);
        Equipment createdEquipment = equipmentService.save(equipmentToAdd);
        EquipmentResponse equipmentResponse = equipmentMapper.equipmentToEquipmentResponse(createdEquipment);
        return ResponseEntity.status(HttpStatus.CREATED).body(equipmentResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<EquipmentResponse> updateEquipment(@PathVariable Long id,
                                                     @Valid @RequestBody EquipmentRequest equipmentRequest) {
        Equipment equipment = equipmentMapper.equipmentRequestToEquipment(equipmentRequest);
        Equipment updatedEquipment = equipmentService.update(id, equipment);
        return ResponseEntity.ok(equipmentMapper.equipmentToEquipmentResponse(updatedEquipment));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<EquipmentResponse> deleteEquipment(@PathVariable Long id) {
        equipmentService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
