package com.example.sarsbackend.service;

import com.example.sarsbackend.exception.EquipmentNotFoundException;
import com.example.sarsbackend.model.Equipment;
import com.example.sarsbackend.repository.EquipmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
@RequiredArgsConstructor
public class EquipmentService {
    private final EquipmentRepository equipmentRepository;

    public List<Equipment> findAll() {
        return equipmentRepository.findAll();
    }

    public Equipment save(Equipment equipment) {
        return equipmentRepository.save(equipment);
    }

    public Equipment update(Long id, Equipment equipment) {
        if (!equipmentRepository.existsById(id)) {
            throw new EquipmentNotFoundException("Equipment with id '" + id + "' doesn't exist");
        }
        equipment.setId(id);
        return save(equipment);
    }

    public void deleteById(Long id) {
        equipmentRepository.deleteById(id);
    }

    public Equipment findById(Long id) {
        return equipmentRepository.findById(id)
                .orElseThrow(() -> new EquipmentNotFoundException("Equipment with id '" + id + "' was not found"));
    }

    public Equipment findById(Collection<Equipment> equipmentCollection, Long id) {
        return equipmentCollection.stream()
                .filter(equipment -> Objects.equals(equipment.getId(), id))
                .findAny()
                .orElseThrow(() -> new EquipmentNotFoundException("Equipment with id '" + id + "' was not found"));
    }
}
