package com.example.sarsbackend.service;

import com.example.sarsbackend.exception.*;
import com.example.sarsbackend.model.*;
import com.example.sarsbackend.repository.CompetitionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class CompetitionService {
    private final CompetitionRepository competitionRepository;
    private final TeamService teamService;
    private final CourtService courtService;
    private final CompetitionValidationService competitionValidationService;

    public List<Competition> findAll() {
        return competitionRepository.findAll();
    }

    public Competition save(Competition competition) {
        Team team1 = teamService.findById(competition.getTeam1().getId());
        Team team2 = teamService.findById(competition.getTeam2().getId());
        Court court = courtService.findById(competition.getCourt().getId());

        competition.setTeam1(team1);
        competition.setTeam2(team2);
        competition.setCourt(court);

        competitionValidationService.validateCompetition(competition);

        return competitionRepository.save(competition);
    }

    public Competition update(Long id, Competition competition) {
        Competition competitionToUpdate = findById(id);

        Team team1 = teamService.findById(competition.getTeam1().getId());
        Team team2 = teamService.findById(competition.getTeam2().getId());
        Court court = courtService.findById(competition.getCourt().getId());

        competitionToUpdate.setTeam1(team1);
        competitionToUpdate.setTeam2(team2);
        competitionToUpdate.setCourt(court);

        competitionValidationService.validateCompetition(competition);

        return competitionRepository.save(competitionToUpdate);
    }

    public void deleteById(Long id) {
        competitionRepository.deleteById(id);
    }

    public Competition findById(Long id) {
        return competitionRepository.findById(id)
                .orElseThrow(() -> new CompetitionNotFoundException("Competition with id '" + id + "' was not found"));
    }
}
