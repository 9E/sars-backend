package com.example.sarsbackend.service;

import com.example.sarsbackend.exception.AccessForbiddenException;
import com.example.sarsbackend.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class UserValidationService {

    private final UserService userService;

    public void validateAccessToUsersResource(Authentication authentication, Long userPathVariableId) {
        if (isAdmin(authentication)) {
            return;
        }
        User authenticatedUser = userService.findByUsername(authentication.getName());

        Long authenticatedUserId = authenticatedUser.getId();
        if (!Objects.equals(authenticatedUserId, userPathVariableId)) {
            throw new AccessForbiddenException(String.format("User with id '%s' doesn't have permission to access" +
                    " user resource with id '%s'", authenticatedUserId, userPathVariableId));
        }
    }

    public boolean isAdmin(Authentication authentication) {
        return authentication.getAuthorities().stream()
                .anyMatch(grantedAuthority -> "ADMIN".equals(grantedAuthority.getAuthority()));
    }
}
