package com.example.sarsbackend.service;

import com.example.sarsbackend.exception.CourtNotFoundException;
import com.example.sarsbackend.model.Court;
import com.example.sarsbackend.repository.CourtRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class CourtService {
    private final CourtRepository courtRepository;

    public List<Court> findAll() {
        return courtRepository.findAll();
    }

    public Court save(Court court) {
        return courtRepository.save(court);
    }

    public Court update(Long id, Court court) {
        if (!courtRepository.existsById(id)) {
            throw new CourtNotFoundException("Court with id '" + id + "' doesn't exist");
        }
        court.setId(id);
        return save(court);
    }

    public void deleteById(Long id) {
        courtRepository.deleteById(id);
    }

    public Court findById(Long id) {
        return courtRepository.findById(id)
                .orElseThrow(() -> new CourtNotFoundException("Court with id '" + id + "' was not found"));
    }
}
