package com.example.sarsbackend.service;

import com.example.sarsbackend.exception.InvalidDatePeriodException;
import com.example.sarsbackend.exception.SameTeamException;
import com.example.sarsbackend.exception.TeamMembersIntersectException;
import com.example.sarsbackend.exception.WrongSportTypeException;
import com.example.sarsbackend.model.Competition;
import com.example.sarsbackend.model.Court;
import com.example.sarsbackend.model.Team;
import com.example.sarsbackend.model.User;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CompetitionValidationService {

    public void validateCompetition(Competition competition) {
        validateTeamMembersIntersect(competition);
        validateNotSameTeam(competition);
        validateDateFromAndDateTo(competition);
        validateTeamSportTypes(competition);
    }

    public void validateTeamMembersIntersect(Competition competition) {
        Team team1 = competition.getTeam1();
        Team team2 = competition.getTeam2();
        Set<User> team1Members = getTeamMembers(team1);
        Set<User> team2Members = getTeamMembers(team2);

        Set<User> intersection = team1Members.stream()
                .filter(team2Members::contains)
                .collect(Collectors.toSet());

        if (!intersection.isEmpty()) {
            throw new TeamMembersIntersectException(String.format("Team {id = %s; name = %s} members intersect with" +
                    "{team id = %s; name = %s} members", team1.getId(), team1.getName(), team2.getId(), team2.getName()));
        }
    }

    private Set<User> getTeamMembers(Team team) {
        return Optional.ofNullable(team.getMembers())
                .map(HashSet::new)
                .orElse(new HashSet<>());
    }

    public void validateNotSameTeam(Competition competition) {
        Team team1 = competition.getTeam1();
        Team team2 = competition.getTeam2();

        if (Objects.equals(team1.getId(), team2.getId())) {
            throw new SameTeamException("Team1 and team2 can't be the same team.");
        }
    }

    public void validateDateFromAndDateTo(Competition competition) {
        Timestamp from = competition.getDateFrom();
        Timestamp to = competition.getDateTo();

        if (from.after(to)) {
            throw new InvalidDatePeriodException("Date to should be after date from");
        }
    }

    public void validateTeamSportTypes(Competition competition) {
        Team team1 = competition.getTeam1();
        Team team2 = competition.getTeam2();
        Court court = competition.getCourt();

        if (!Objects.equals(competition.getSportType(), team1.getSportType())) {
            throw new WrongSportTypeException(String.format("Team1 has to be of '%s' sport type",
                    competition.getSportType()));
        }
        if (!Objects.equals(competition.getSportType(), team2.getSportType())) {
            throw new WrongSportTypeException(String.format("Team2 has to be of '%s' sport type",
                    competition.getSportType()));
        }
        if (!Objects.equals(competition.getSportType(), court.getSportType())) {
            throw new WrongSportTypeException(String.format("Court has to be of '%s' sport type",
                    competition.getSportType()));
        }
    }
}
