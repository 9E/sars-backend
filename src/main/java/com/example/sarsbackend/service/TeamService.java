package com.example.sarsbackend.service;

import com.example.sarsbackend.exception.TeamNotFoundException;
import com.example.sarsbackend.model.Competition;
import com.example.sarsbackend.model.Team;
import com.example.sarsbackend.model.User;
import com.example.sarsbackend.repository.TeamRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class TeamService {
    private final TeamRepository teamRepository;

    public List<Team> findAll() {
        return teamRepository.findAll();
    }

    public Team save(Team team, User creator) {
        team.setCreatedBy(creator);
        return teamRepository.save(team);
    }

    public Team update(Long id, Team team) {
        Team teamToUpdate = findById(id);
        teamToUpdate.setName(team.getName());
        teamToUpdate.setSportType(team.getSportType());
        return teamRepository.save(teamToUpdate);
    }

    public void deleteById(Long id) {
        Team team = findById(id);

        for (User user : team.getMembers()) {
            user.getTeams().remove(team);
        }
        for (Competition competition : team.getCompetitionsAsTeam1()) {
             competition.setTeam1(null);
        }
        for (Competition competition : team.getCompetitionsAsTeam2()) {
             competition.setTeam2(null);
        }

        teamRepository.delete(team);
    }

    public Team findById(Long id) {
        return teamRepository.findById(id)
                .orElseThrow(() -> new TeamNotFoundException("Team with id '" + id + "' was not found"));
    }
}
