package com.example.sarsbackend.service;

import com.example.sarsbackend.exception.UserNotFoundException;
import com.example.sarsbackend.model.Role;
import com.example.sarsbackend.model.Team;
import com.example.sarsbackend.model.User;
import com.example.sarsbackend.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findByUsername(username);
        List<SimpleGrantedAuthority> authorities = List.of(new SimpleGrantedAuthority(user.getRole().toString()));

        return new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                user.getPassword(),
                authorities
        );
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User updateRole(Long id, String roleName) {
        Role role = Role.valueOf(roleName);
        User user = findById(id);
        user.setRole(role);
        return user;
    }

    public User update(Long id, User user) {
        User userToUpdate = findById(id);
        userToUpdate.setUsername(user.getUsername());
        userToUpdate.setPassword(user.getPassword());
        userToUpdate.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(userToUpdate);
    }

    public User save(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setCreatedAt(new Timestamp(System.currentTimeMillis()));
        return userRepository.save(user);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException("User with username '" + username + "' was not found"));
    }

    public void deleteById(Long id) {
        User user = findById(id);

        for (Team team : user.getTeams()) {
            team.getMembers().remove(user);
        }
        for (Team team : user.getCreatedTeams()) {
            team.setCreatedBy(null);
        }
        user.setCreatedTeams(null);

        userRepository.deleteById(id);
    }

    public User findById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User with id '" + id + "' was not found"));
    }

    public void addDefaultAdminUser() {
        try {
            User adminUser = findByUsername("admin");
            if (adminUser.getRole() != Role.ADMIN) {
                adminUser.setRole(Role.ADMIN);
            }
        } catch (UserNotFoundException e) {
            save(new User("admin", "admin", Role.ADMIN));
        }
    }
}
