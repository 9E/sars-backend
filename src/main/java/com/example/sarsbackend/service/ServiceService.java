package com.example.sarsbackend.service;

import com.example.sarsbackend.exception.ServiceNotFoundException;
import com.example.sarsbackend.model.Court;
import com.example.sarsbackend.model.Equipment;
import com.example.sarsbackend.model.Service;
import com.example.sarsbackend.repository.ServiceRepository;
import lombok.RequiredArgsConstructor;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@org.springframework.stereotype.Service
@Transactional
@RequiredArgsConstructor
public class ServiceService {
    private final ServiceRepository serviceRepository;
    private final EquipmentService equipmentService;
    private final CourtService courtService;

    private final double HOUR_IN_MILLIS = 60 * 60 * 1000;

    public Service updateEquipment(Long serviceId, Long equipmentId) {
        Service service = findById(serviceId);
        Equipment equipment = equipmentService.findById(equipmentId);

        equipment.setService(service);
        service.getEquipmentSet().add(equipment);

        return service;
    }

    public Service deleteEquipment(Long serviceId, Long equipmentId) {
        Service service = findById(serviceId);
        Equipment equipment = equipmentService.findById(service.getEquipmentSet(), equipmentId);

        equipment.setService(null);
        service.getEquipmentSet().remove(equipment);

        return service;
    }

    public List<Service> findAll() {
        return serviceRepository.findAll();
    }

    public Service save(Service service) {
        return serviceRepository.save(service);
    }

    public Service update(Long id, Service service) {
        Service serviceToUpdate = findById(id);
        Court court = getCourt(service);

        serviceToUpdate.setName(service.getName());
        serviceToUpdate.setCourt(court);

        return serviceRepository.save(serviceToUpdate);
    }

    private Court getCourt(Service service) {
        return Optional.ofNullable(service.getCourt())
                .map(Court::getId)
                .map(courtService::findById)
                .orElse(null);
    }

    public void deleteById(Long id) {
        Service service = findById(id);
        service.getEquipmentSet().forEach(equipment -> equipment.setService(null));
        serviceRepository.delete(service);
    }

    public double getServiceTotalPrice(Long serviceId, Timestamp from, Timestamp to) {
        Service service = findById(serviceId);
        double hoursToReserveFor = (to.getTime() - from.getTime()) / HOUR_IN_MILLIS;
        double totalPrice = 0;

        if (service.getCourt() != null && service.getCourt().getPricePerHour() != null) {
            double courtPricePerHour = service.getCourt().getPricePerHour();
            totalPrice += courtPricePerHour * hoursToReserveFor;
        }
        totalPrice += getAllEquipmentPricePerHour(service) * hoursToReserveFor;

        return totalPrice;
    }

    private Double getAllEquipmentPricePerHour(Service service) {
        return service.getEquipmentSet().stream()
                .map(Equipment::getPricePerHour)
                .filter(Objects::nonNull)
                .reduce(0d, Double::sum);
    }

    public Service findById(Long id) {
        return serviceRepository.findById(id)
                .orElseThrow(() -> new ServiceNotFoundException("Service with id '" + id + "' was not found"));
    }
}
