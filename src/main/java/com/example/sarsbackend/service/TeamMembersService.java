package com.example.sarsbackend.service;

import com.example.sarsbackend.model.Team;
import com.example.sarsbackend.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class TeamMembersService {
    private final UserService userService;
    private final TeamService teamService;

    public User addTeamToUser(Long userId, Long teamId) {
        User user = userService.findById(userId);
        Team team = teamService.findById(teamId);

        user.getTeams().add(team);
        team.getMembers().add(user);

        return user;
    }

    public User removeTeamFromUser(Long userId, Long teamId) {
        User user = userService.findById(userId);
        Team team = teamService.findById(teamId);

        user.getTeams().remove(team);
        team.getMembers().remove(user);

        return user;
    }
}
