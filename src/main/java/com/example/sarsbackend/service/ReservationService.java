package com.example.sarsbackend.service;

import com.example.sarsbackend.exception.InvalidDatePeriodException;
import com.example.sarsbackend.exception.ReservationNotFoundException;
import com.example.sarsbackend.model.Reservation;
import com.example.sarsbackend.model.Service;
import com.example.sarsbackend.model.User;
import com.example.sarsbackend.repository.ReservationRepository;
import lombok.RequiredArgsConstructor;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@org.springframework.stereotype.Service
@Transactional
@RequiredArgsConstructor
public class ReservationService {
    private final ReservationRepository reservationRepository;
    private final UserService userService;
    private final ServiceService serviceService;

    public Reservation createReservation(Reservation reservation) {
        validateReservationDatePeriod(reservation);
        User user = userService.findById(reservation.getUser().getId());
        Service service = serviceService.findById(reservation.getService().getId());

        reservation.setUser(user);
        reservation.setService(service);
        user.getReservations().add(reservation);
        service.getReservations().add(reservation);

        double totalPrice = serviceService.getServiceTotalPrice(service.getId(),
                reservation.getReservedFrom(), reservation.getReservedTo());
        reservation.setTotalPrice(totalPrice);

        return reservationRepository.save(reservation);
    }

    public void validateReservationDatePeriod(Reservation reservation) {
        Timestamp from = reservation.getReservedFrom();
        Timestamp to = reservation.getReservedTo();
        if (from.after(to)) {
            throw new InvalidDatePeriodException("Date to should be after date from");
        }
    }

    public void deleteById(Long id) {
        Reservation reservation = findById(id);
        User user = reservation.getUser();
        Service service = reservation.getService();

        user.getReservations().remove(reservation);
        service.getReservations().remove(reservation);
        reservation.setUser(null);
        reservation.setService(null);

        reservationRepository.delete(reservation);
    }

    public Reservation findById(Long id) {
        return reservationRepository.findById(id)
                .orElseThrow(() -> new ReservationNotFoundException(
                        String.format("Reservation with given id=%s wasn't found.", id)));
    }

    public List<Reservation> findAll() {
        return reservationRepository.findAll();
    }
}
