package com.example.sarsbackend.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {
    private final UserDetailsService userDetailsService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        var customAuthenticationFilter = new CustomAuthenticationFilter(authenticationManagerBean());
        customAuthenticationFilter.setFilterProcessesUrl("/api/v1/login");

        http.sessionManagement().sessionCreationPolicy(STATELESS)
                .and().authorizeRequests().antMatchers(HttpMethod.POST, "/api/v1/users").permitAll()
                .antMatchers("/api/v1/users/{id}/role/**").hasAuthority("ADMIN")
                /* TODO: rethink permissions once again later.
                    Also, certain permitAll() or hasAnyAuthority("USER", "ADMIN") are temporary. */
                .antMatchers( "/api/v1/users/{userId}/services/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/users/{id}/**").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/v1/users/{id}/**").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/v1/users/{id}/**").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.GET, "/api/v1/users/**").hasAuthority("ADMIN")
                .antMatchers("/api/v1/roles/**").hasAuthority("ADMIN")
                .antMatchers("/api/v1/courts/**").hasAuthority("ADMIN")
                .antMatchers("/api/v1/court-types/**").hasAuthority("ADMIN")
                .antMatchers("/api/v1/equipment/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.GET,"/api/v1/services/**").permitAll()
                .antMatchers( "/api/v1/services/**").hasAuthority("ADMIN")
                .antMatchers( "/api/v1/reservations/**").permitAll()
                .antMatchers( "/api/v1/teams/**").hasAnyAuthority("USER", "ADMIN")
                .antMatchers( "/api/v1/competitions/**").hasAnyAuthority("USER", "ADMIN")
                .anyRequest().authenticated()
                .and().addFilter(customAuthenticationFilter)
                .addFilterBefore(new CustomAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);

        http.cors().and().csrf().disable();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedMethods("*");
            }
        };
    }
}
