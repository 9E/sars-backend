package com.example.sarsbackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidDatePeriodException extends RuntimeException {

    public InvalidDatePeriodException(String message) {
        super(message);
    }
}
